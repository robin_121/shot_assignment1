// SHOT on Goal 2019. PLEASE USE THIS VERSION FOR THE ASSIGNMENT.
// This version is identical to the original demo version except where indicated by comments starting "//***SHOT".
// The additional code conveniently tells you when you've broken it during your optimisation efforts.
// This version is currently tailored for a 12m kick distance for demo purposes. Please edit it where requested 
// to tailor it to your allotted kick distance.

/* In the game of rugby a touchdown is converted by booting the ball over the 3m high goal cross bar from some point
on the pitch, but orthogonal to the try line where the touchdown occurred.
This program calculates how fast and at what vertical angle you would have to kick the ball to get it over the cross bar from
a set distance (which is input).
It does this by cycling through a series of angles for an initial kick speed, and figuring out using standard trajectory equations
whether the ball would make it over the bar (ignoring factors such as air resistance, ball spin, air
density, and so on). If none is found an increased kick speed is used and the cycle restarted until the first working combination is found.
Using the kick speed and angle thus found, a series of x:y
coordinates of the ball's trajectory path are calculated and saved in an array. It is assumed that the ball is kicked on target
and goes through the uprights.
A 'display' function then plots these on a rugby-stylised graph for inspection (see screen layout later).
Units are (m) metres, (s) seconds, (deg) degrees, all single float values.

Adrian Oram, Nov 2018 (not a rugby supporter, so don't ask!)
*/




// ------- comments marked **SHOT** are places where i am describing an optimisation i have done -------------


#define yourName     "Robin Onians"
#define yourTeamName "Fulchester RC"
#define yourKickDistance 34.5			//*** Alter this to your assigned kick distance (units are metres).
const float actualAngle(16.0f);			//*** Edit these to your known correct values for your kick distance 
const float actualSpeed(31.5F);			

#include <fstream>			// for file I/O
#include <windows.h>
#include <math.h>			// for COS, TAN, etc
#include <string>
#include <sstream>		
#include <iostream>
#include <iomanip>
#include "omp.h"			// for omp
#include <xmmintrin.h>		// for mmx registers
using namespace std;



// Function Prototypes
template <class T>
string ToString(T);
void getDistanceToKick(float*);
//bool findSpeedAndAngle(float*, float*, float);
//void generateFlightPath(float, float);
void showFlightPathResults(float, float, float);
void logData(const float&, const float&);




// Timing stuff
__declspec(align(32)) const int repeats = 10000;					//*** Timing - Repeat each function to help avoid exceptions, and record fastest times seen.
const __int64 bigTicks = 1000000;									//*** Timing - big tick count to ensure fastest found.

_inline unsigned __int64 GetTimeStamp(__int64*);					//*** Timing
__declspec(align(64)) double SpeedAndAngleTme;						//*** Timing - The important numbers!
__declspec(align(64)) double GenerateFlightPathTime;
__int64 tick_count;													//*** Timing - raw clock count in ticks
__int64 minTicks(bigTicks);											//*** Timing - set minimum ticks to a high value
__int64 start_time, end_time;										//*** Timing 

// Some constants
__declspec(align(32)) const float g(9.81F);							// (m/s/s) gravity
__declspec(align(32)) const float Pi(3.14159265358979323846F);		// This value stolen from M_PI defined in Math.h - used to convert degrees to radians
__declspec(align(32)) const float dataEnd = -1.0F;					// End of data marker
__declspec(align(32)) const int x = 0;								// coordinate system
__declspec(align(32)) const int y = 1;
#define SPACE ' '
__declspec(align(64)) const double micro(1.0e6F);					// scaling factor to give microseconds
__declspec(align(64)) const double milli(1.0e3F);					// scaling factor to give milliseconds

// Kick metrics
__declspec(align(32)) const float minAngle(0.261799F);				//** SHOT ** (Converted minAngle and maxAngle to radions from Degrees to avoid confersion maths within function)
__declspec(align(32)) const float maxAngle(0.523599F);				//** SHOT ** most results fall in this range.

__declspec(align(32)) const float minSpeed(5.0F);					// (m/s) 
__declspec(align(32)) const float maxSpeed(32.0F);					// (m/s) 
__declspec(align(32)) const float maxHeight(8.5F);					// (m)   trajectories above this height can't be displayed (out the park!)
__declspec(align(32)) float kickSpeed;								// (m/s) to be calculated
__declspec(align(32)) float kickAngle;								// (deg) to be calculated

// Pitch metrics
__declspec(align(32)) const float minDistanceToGoal(5.0F);			// (m) Probably too close! Might get charged down!
__declspec(align(32)) const float maxDistanceToGoal(50.0F);			// (m) This is almost half a standard rugby pitch length.
__declspec(align(32)) const float crossBarHeight(3.0F);				// (m) 3m is the standard rugby cross bar height.
__declspec(align(32)) const float margin(0.5F);						// (m) allow for a margin of error getting it over.
__declspec(align(32)) const float goalPostHeight(7.0F);				// (m) Upto 16m usually. Don't make higher, it won't fit on display!
__declspec(align(32)) float distanceToGoal;							// (m) Kicking distance, input by user in this version.

// Final flight path of ball as a series of x,y coordinates
__declspec(align(32)) const float deltaDs(0.5F);					// (m) need a coordinate every tick of this distance, plus increment used for speed.
__declspec(align(32)) const float deltaDa(0.00872665F);				// ** SHOT ** degree increment converted to Radions
__declspec(align(32)) const float deltaY(0.25F);					// (m) increment in the height direction (vertical exaggeration of x2)
__declspec(align(32)) const float yScale = 1.0F / deltaY;			// vertical scaling factor
__declspec(align(32)) const float xScale = 1.0F / deltaDs;			// horizontal scaling factor

__declspec(align(32)) const int maxDataPoints = (int)((maxDistanceToGoal + 2.0F) / deltaDs);	// =104, calculate a data point for each 0.5 metre along (+2.0 so that ball appears beyond the goal at maxDistance)

__declspec(align(32)) float flightPath[104 + 1][2];					// x,y coords (m,m) of ball flight. The sequence terminates with 'dataEnd' if fewer than maxDataPoints used.




//----- Time to solution declarations -----							// **SHOT** pre-declare (and initialise) variables out-side the timed loop to prevent declaration/initialisation times times

__declspec(align(32)) float cosLookUp[]								// **SHOT** pre-defined cos increments to prevent maths computation at runtime
{
	{1.8660258055},{1.8571677208},{1.8480485678},{1.8386708498},{1.8290379047},{1.8191524744},{1.8090173006},{1.7986358404},{1.7880110741},{1.7771464586},
	{1.7660448551},{1.7547098398},{1.7431451082},{1.7313539982},{1.7193400860},{1.7071070671},{1.6946586370},{1.6819986105},{1.6691309214},{1.6560592651},
	{1.6427879333},{1.6293205023},{1.6156617403},{1.6018152237},{1.5877854824},{1.5735765696},{1.5591930151},{1.5446391106},{1.5299195051},{1.5150381327}
};
__declspec(align(32)) float tanLookUp[]								// **SHOT** pre-defined tan increments to prevent maths computation at runtime
{
	{0.2679487765},{0.2773241401},{0.2867449820},{0.2962131202},{0.3057303131},{0.3152984381},{0.3249193430},{0.3345949650},{0.3443272710},{0.3541182578},
	{0.3639699221},{0.3738843799},{0.3838637471},{0.3939101994},{0.4040259421},{0.4142132998},{0.4244745672},{0.4348121285},{0.4452284575},{0.4557260275},
	{0.4663074315},{0.4769753218},{0.4877323806},{0.4985814393},{0.5095252991},{0.5205668807},{0.5317092538},{0.5429555774},{0.5543088913},{0.5657726526}
};

__declspec(align(32)) float speed;
__declspec(align(32)) float angle;
__declspec(align(32)) float gravityEquations;
__declspec(align(32)) float nextSpeed;
__declspec(align(32)) float nextAngle;
__m128 height = _mm_set_ps(0.f, 0.f, 0.f, 0.f);
__m128 crossbarHieght = _mm_set1_ps(crossBarHeight + margin);
__m128 comparison = _mm_set_ps(0.f, 0.f, 0.f, 0.f);
__declspec(align(32)) float heightEquation1;
__declspec(align(32)) float heightEquation2;

//-----------------------------------------






//****************************************** MAIN *************************************************************************
int main(void)
{
	LARGE_INTEGER freq;												//*** Timing
	QueryPerformanceFrequency(&freq);								//*** Timing - get CPU clock frequency
	double frequency = (double)(freq.QuadPart * 1000);				//*** Timing - scale to GHz & cast to a float for later use

	bool foundCombo(false);
	//getDistanceToKick(&distanceToGoal);							// Prompt for distance to goal.
	distanceToGoal = yourKickDistance;								// Convenient way of getting your kick distance in!

	std::cout << "\nYou entered " << distanceToGoal << " metres. Looking for solution for kick speed and angle...";
	

	//************************************* Find Speed and Angle *********************************************************
	//
	// Figure out the first working combination of Speed and Angle.
	// Using the lowest kick speed to begin with, increment through the angles from low to high until one is found that gets 
	// the ball over the bar. If none found increase the speed and repeat. Rationale: it's better to use as little 
	// energy in the kick as possible! DeltaD is used as the increment for both angle and speed.


	float dist = distanceToGoal;
	gravityEquations = (-g * dist*dist);							// **SHOT** initialise the gravity equations outside the timed loop

	for (int reps(0); reps < repeats; ++reps)
	{

		GetTimeStamp(&start_time);									//*** Timing - read CPU clock

		//****************************************************************************************************************

		foundCombo = false;											// Found combination of speed and angle that gets ball over bar?

		speed = kickSpeed;
		angle = kickAngle;
		nextAngle = minAngle;										// Start with shallowest angle...
		int count = 0;												// For lookup table index

		while (!(foundCombo || (nextAngle > maxAngle)))				// **SHOT** de Morgan's Theory
		{
			nextSpeed = minSpeed;									// reset minimum speed 

			heightEquation1 = (cosLookUp[count]);
			heightEquation2 = (dist * tanLookUp[count]);

			count++;

			while (!(foundCombo || ((nextSpeed + deltaDs *3) > maxSpeed))) // **SHOT** de Morgan's Theory
			{
				// Figure out height of ball at goal post distance (x), using classic trajectory equation...

				// If this is > cross bar height (plus any margin allowed) then result! 
				// Note: height could become negative if ball hits ground short of posts (and theoretically keeps going underground!).
				// Note: Max horizontal distance can be calculated from = (speed^2) * sin(2*angle)/g

				// **SHOT** height = (((gravityEquations) / (heightEquation1 * (nextSpeed*nextSpeed))) + heightEquation2) instead of height =  (-g * x^2)/(2 * cos(angle)^2 * speed^2) + (x * tan(angle))
				// **SHOT** using xmm registers to store 4 heights in one __m128 height variable
				
				// **SHOT** original equation -- ((gravityEquations) / (heightEquation1 * (nextSpeed*nextSpeed))) + heightEquation2; -- seperate out the height equation to aid SIMD and parallelism 
				// (do 4 bits of maths at the same time)

				height = _mm_set_ps(nextSpeed,
									nextSpeed + deltaDs,
									nextSpeed + deltaDs * 2,
									nextSpeed + deltaDs * 3);

				height = _mm_mul_ps(height, height);				//nextSpeed squared

				__m128 part1 = _mm_set1_ps(heightEquation1);		
				height = _mm_mul_ps(height, part1);					//nextSpeed squared * cosPart

				__m128 part2 = _mm_set1_ps(gravityEquations);		
				height = _mm_div_ps(part2, height);					//gravityEquations / nextSpeed squared * cosPart

				__m128 part3 = _mm_set1_ps(heightEquation2);
				height = _mm_add_ps(height, part3);					//gravityEquations / nextSpeed squared * cosPart + heightEquation2

																	
				comparison = _mm_cmplt_ps(height, crossbarHieght);	// **SHOT** now we can compare 4 hieghts at the same time. Comparison will store all zeros if any of the comparisons result true

				if (comparison.m128_f32[0] != 0)
				{
					nextSpeed += deltaDs*3;							// Otherwise try next speed up (+0.5 m/s).
				}
				else //success
				{
					//check at which height the ball passes over the cross bar and assign relative speeds
					if (height.m128_f32[3] > crossBarHeight + margin)
					{
						speed = nextSpeed;							// Record the working combination...

					}
					else if (height.m128_f32[2] > crossBarHeight + margin)
					{
						speed = nextSpeed + deltaDs;			

					}
					else if (height.m128_f32[1] > crossBarHeight + margin)
					{
						speed = nextSpeed + deltaDs * 2;			

					}
					else if (height.m128_f32[0] > crossBarHeight + margin)
					{
						speed = nextSpeed + deltaDs * 3;	
					}
					angle = nextAngle;
					foundCombo = true;								// ... and stop looking.
					break;
				}
			}
			nextAngle += deltaDa;									// no joy, try next angle up (+0.5 degrees).
		}

		kickSpeed = speed;
		kickAngle = angle;

		//*********************************************************************************************************

		GetTimeStamp(&end_time);									//*** Timing - read CPU clock
		tick_count = (end_time - start_time);						//*** Timing - calc ticks used
		if (tick_count < minTicks)									//*** Timing - Quickest so far?
			minTicks = tick_count;									//*** Timing - Set new quickest speed
	}


	SpeedAndAngleTme = ((double)minTicks * micro) / frequency;		//*** Timing - get time in micro seconds
	minTicks = bigTicks;											//*** Timing - reset minTicks

	if (foundCombo)
	{
		std::cout << " solution found. Generating ball's flight path for display...";

		//************************************* Generate flight path coords **********************************************
		//
		// With metrics found, calculate the flight path coords. Uses 'flightPath[104][2]' array as global.
		
		float tanA = tan(angle);									// **SHOT** loop invariant code motion
		float sum = (2.0F * cos(angle) * cos(angle) * (speed * speed));

		for (int reps(0); reps < repeats; ++reps)
		{
			GetTimeStamp(&start_time);								//*** Timing - read CPU clock

			//************************************************************************************************************
			
			// generateFlightPath(kickSpeed, kickAngle);			// **SHOT** function has been inlined, no need for any calling procedure or conventions.
																	

			float yValue(0.001F);									// ball is sitting on a tee just above the ground begin with, of course!
			float xValue(0.0F);										// ...and hasn't moved yet.

			int i(0);

			for (; i < maxDataPoints && (yValue > 0.0) && (yValue <= maxHeight); ++i)	// If height goes negative or too high, STOP!
			{
																	// **SHOT** for loop unroll, unrolled the loop an even amount of times
				if (i < maxDataPoints && (yValue > 0.0) && (yValue <= maxHeight))
				{
					flightPath[i][x] = xValue;						// store data points
					flightPath[i][y] = yValue;
					xValue += deltaDs;								// do for each increment tick across the pitch
					++i;
																	// find the 'y' (height) for each 'x' distance using the angle and speed previously found (same equation as above)

					yValue = ((-g * xValue * xValue) / sum) + (xValue * tanA);
				}
				if (i < maxDataPoints && (yValue > 0.0) && (yValue <= maxHeight))
				{
					flightPath[i][x] = xValue;
					flightPath[i][y] = yValue;
					xValue += deltaDs;
					++i;
					yValue = ((-g * xValue * xValue) / sum) + (xValue * tanA);
				}
				if (i < maxDataPoints && (yValue > 0.0) && (yValue <= maxHeight))
				{
					flightPath[i][x] = xValue;
					flightPath[i][y] = yValue;
					xValue += deltaDs;
					++i;
					yValue = ((-g * xValue * xValue) / sum) + (xValue * tanA);
				}
				if (i < maxDataPoints && (yValue > 0.0) && (yValue <= maxHeight))
				{
					flightPath[i][x] = xValue;
					flightPath[i][y] = yValue;
					xValue += deltaDs;			
					++i;
					yValue = ((-g * xValue * xValue) / sum) + (xValue * tanA);
				}
				if (i < maxDataPoints && (yValue > 0.0) && (yValue <= maxHeight))
				{
					flightPath[i][x] = xValue;	
					flightPath[i][y] = yValue;
					xValue += deltaDs;			
					++i;
					yValue = ((-g * xValue * xValue) / sum) + (xValue * tanA);
				}
				if (i < maxDataPoints && (yValue > 0.0) && (yValue <= maxHeight))
				{
					flightPath[i][x] = xValue;	
					flightPath[i][y] = yValue;
					xValue += deltaDs;			
					++i;
					yValue = ((-g * xValue * xValue) / sum) + (xValue * tanA);
				}
				if (i < maxDataPoints && (yValue > 0.0) && (yValue <= maxHeight))
				{
					flightPath[i][x] = xValue;	
					flightPath[i][y] = yValue;
					xValue += deltaDs;			
					++i;
					yValue = ((-g * xValue * xValue) / sum) + (xValue * tanA);
				}
				if (i < maxDataPoints && (yValue > 0.0) && (yValue <= maxHeight))
				{
					flightPath[i][x] = xValue;	
					flightPath[i][y] = yValue;
					xValue += deltaDs;			
					++i;
					yValue = ((-g * xValue * xValue) / sum) + (xValue * tanA);
				}
				if (i < maxDataPoints && (yValue > 0.0) && (yValue <= maxHeight))
				{
					flightPath[i][x] = xValue;	
					flightPath[i][y] = yValue;
					xValue += deltaDs;			
					++i;
					yValue = ((-g * xValue * xValue) / sum) + (xValue * tanA);
				}
				if (i < maxDataPoints && (yValue > 0.0) && (yValue <= maxHeight))
				{
					flightPath[i][x] = xValue;	
					flightPath[i][y] = yValue;
					xValue += deltaDs;			
					++i;
					yValue = ((-g * xValue * xValue) / sum) + (xValue * tanA);
				}
				if (i < maxDataPoints && (yValue > 0.0) && (yValue <= maxHeight))
				{
					flightPath[i][x] = xValue;	
					flightPath[i][y] = yValue;
					xValue += deltaDs;		
					++i;
					yValue = ((-g * xValue * xValue) / sum) + (xValue * tanA);
				}
				if (i < maxDataPoints && (yValue > 0.0) && (yValue <= maxHeight))
				{
					flightPath[i][x] = xValue;	
					flightPath[i][y] = yValue;
					xValue += deltaDs;			
					++i;
					yValue = ((-g * xValue * xValue) / sum) + (xValue * tanA);
				}
				if (i < maxDataPoints && (yValue > 0.0) && (yValue <= maxHeight))
				{
					flightPath[i][x] = xValue;	
					flightPath[i][y] = yValue;
					xValue += deltaDs;			
					//++i; (final increment done within for loop)
					yValue = ((-g * xValue * xValue) / sum) + (xValue * tanA);
				}

			}
			// Finished generating required data points, now mark end-of-data with -1.0 (dataEnd)
			flightPath[i][x] = dataEnd;
			flightPath[i][y] = dataEnd;

			//*********************************************************************************************************

			GetTimeStamp(&end_time);									//*** Timing - read CPU clock
			tick_count = (end_time - start_time);						//*** Timing - calc ticks used
			if (tick_count < minTicks)									//*** Timing - Quickest so far?
				minTicks = tick_count;									//*** Timing - Set new quickest speed
		}

		GenerateFlightPathTime = ((double)minTicks * micro) / frequency;//*** Timing - get time in micro seconds

		showFlightPathResults(kickSpeed, kickAngle, distanceToGoal);
	}
	else std::cout << "no solution found.\n";

	std::cout << "\n\nDone...";
	std::system("PAUSE");		// hold console on PC.

	return(0);
}
//************************************* END MAIN ********************************************************************






//************************************ Supporting functions - DO NOT ALTER! ***************************************
//*****************************************************************************************************************

// Display a stylised flightpath of ball etc. Memory is cheap so lots of replicated chars used for simplicity. 
// The string array below holds all the fixed text - the data points and other information are superimposed on it.
// The graph displays with roughly a 5x vertical exaggeration, the scale ticks have a 2x vertical exaggeration.
// PLEASE DON'T ALTER ANYTHING IN THIS FUNCTION.

void showFlightPathResults(float speed, float angle, float distanceToGoal)
{
	angle = angle * (180 / Pi);
	string YaxisTitle1 = "\n\n Height (m) above pitch\n in 0.25m increments\t\t\t\t\t\tSHOT ON GOAL CALCULATOR ";
	string YaxisTitle2 = "                           \n (Vert. exag.~= x5) \t\t\t\t\t\t~~~~~~~~~~~~~~~~~~~~~~~\n\n";
	string XaxisTitle1 = "\n\t\t\tDistance (m) to ";
	string XaxisTitle2 = "'s goal in 0.5m increments\n\n";
	string goalPostTitle = "+ = cross bar";

	const int leftMargin = 13;		// Number of chars from left side of screen to '0' position, from where ball is kicked.
	const int bottomMargin = 2;		// ditto from bottom to pitch level.
	const int resultsTextPos = 29;	// How far in to position results data.
	const int graphLines = 37;		// How many lines of text there are in the array below.
	const int pitchLevel = graphLines - bottomMargin - 1; // get stuff on to pitch directly.

	// This array = 4.4KB ish. There are 121 characters in each line + '\0' terminator.

	// This string array is treated as a 2-D array of chars, with row [0] at the top. The 'X's are replaced by
	// the performance data.

	string graphDisplay[] = {

		" 8.5 | Time to solution (us)  X                                                                                         \n",
		"     | Time generating  (us)  X                                                                                         \n",
		" 8.0 |      Kick angle (deg)  X             .         .         .         .         .         .         .         .     \n",
		"     |      Kick speed (m/s)  X                                                                                         \n",
		" 7.5 |         X  Iterations                                                                                           \n",
		"     |                                                                                                                 \n",
		" 7.0 |       .         .         .         .         .         .         .         .         .         .         .     \n",
		"     |                                                                                                                 \n",
		" 6.5 |                                                                                                                 \n",
		"     |                                                                                                                 \n",
		" 6.0 |       .         .         .         .         .         .         .         .         .         .         .     \n",
		"     |                                                                                                                 \n",
		" 5.5 |                                                                                                                 \n",
		"     |                                                                                                                 \n",
		" 5.0 |       .         .         .         .         .         .         .         .         .         .         .     \n",
		"     |                                                                                                                 \n",
		" 4.5 |                                                                                                                 \n",
		"     |                                                                                                                 \n",
		" 4.0 |       .         .         .         .         .         .         .         .         .         .         .     \n",
		"     |                                                                                                                 \n",
		" 3.5 |                                                                                                                 \n",
		"     |                                                                                                                 \n",
		" 3.0 |       .         .         .         .         .         .         .         .         .         .         .     \n",
		"     |                                                                                                                 \n",
		" 2.5 |                                                                                                                 \n",
		"     |                                                                                                                 \n",
		" 2.0 |       .         .         .         .         .         .         .         .         .         .         .     \n",
		"     |                                                                                                                 \n",
		" 1.5 |                                                                                                                 \n",
		"     |                                                                                                                 \n",
		" 1.0 |  _Q             .         .         .         .         .         .         .         .         .         .     \n",
		"     | | |\\_o                                                                                                          \n",
		" 0.5 | o |____#                                                                                                        \n",
		"     |  /                                                                                                              \n",
		" 0.0 | /    BLAT!!                                                                                                     \n",
		"     +-##----^+++++++++|+++++++++|+++++++++|+++++++++|+++++++++|+++++++++|+++++++++|+++++++++|+++++++++|+++++++++|+++++\n",
		"             0         5         10        15        20        25        30        35        40        45        50    \n"
	};// Nb. the double '\\' for the kicker's arm isn't a mistake. See actual output...

	const char goalPost = '|';
	const char crossBar = '+';
	const char ball = 'O';

	// Insert timing and other data...
	const int fieldWidth = 6;	//...convert data to a string and insert in display; SPACE replaces the end NULL of the string.
	graphDisplay[0].replace(resultsTextPos, fieldWidth, ToString(SpeedAndAngleTme) + SPACE);		// Time for finding the solution
	graphDisplay[1].replace(resultsTextPos, fieldWidth, ToString(GenerateFlightPathTime) + SPACE);		// Time for generating data points
	graphDisplay[2].replace(resultsTextPos, fieldWidth, ToString(angle) + SPACE);		// Kick angle found
	graphDisplay[3].replace(resultsTextPos, fieldWidth, ToString(speed) + SPACE);		// Kick speed found
	graphDisplay[4].replace(11, fieldWidth, ToString(repeats) + SPACE);		// # of times calcs repeated

	//***SHOT Check angle & speed figures and highlight disparities. ********************************************************************
	//***SHOT For example  12.0 metres distance, we're expecting Speed=30.5m/s and angle=20.0 degrees.
	//***SHOT We can check for equalities with these floats as they're definite figures.

	std::round(angle) == actualAngle ? graphDisplay[2].replace(resultsTextPos + 8, 2, "OK   ")
		: graphDisplay[2].replace(resultsTextPos + 8, fieldWidth + 13, "X " + ToString(actualAngle) + " expected ");	//***SHOT check angle
	speed == actualSpeed ? graphDisplay[3].replace(resultsTextPos + 8, 2, "OK")
		: graphDisplay[3].replace(resultsTextPos + 8, fieldWidth + 13, "X " + ToString(actualSpeed) + " expected ");	//***SHOT check speed
	//***SHOT ***************************************************************************************************************************


	//***SHOT Check each data point generated and display any incorrect ones as 'x' and plot the correct ones as 'ball'.  
	//***SHOT Only corrects for the data points actually produced, so may appear short.
	//***SHOT The example data set below contains the correct data points generated from the flight data for distance = 12.0m. You should find the
	//***SHOT equivalent set of data for your allocated kick distance and copy and paste it over this (unless you have kick distance 12.0m, of course!)
	//***SHOT The file "AllFlightPathData.txt" on Blackboard contains all of the required data sets. You may need to reformat it to make it look OK.

	int actualPoints[][2] = {

	{13,34}, {14,34}, {15,33}, {16,33}, {17,32}, {18,32}, {19,31}, {20,31}, {21,30}, {22,30}, {23,29}, {24,29}, {25,28}, {26,28}, {27,28}, {28,27}, 
	{29,27}, {30,26}, {31,26}, {32,26}, {33,25}, {34,25}, {35,24}, {36,24}, {37,24}, {38,24}, {39,23}, {40,23}, {41,23}, {42,22}, {43,22}, {44,22}, 
	{45,22}, {46,21}, {47,21}, {48,21}, {49,21}, {50,21}, {51,20}, {52,20}, {53,20}, {54,20}, {55,20}, {56,20}, {57,20}, {58,20}, {59,19}, {60,19}, 
	{61,19}, {62,19}, {63,19}, {64,19}, {65,19}, {66,19}, {67,19}, {68,19}, {69,19}, {70,19}, {71,19}, {72,19}, {73,19}, {74,19}, {75,20}, {76,20}, 
	{77,20}, {78,20}, {79,20}, {80,20}, {81,20}, {82,20}, {83,21}, {84,21}, {85,21}, {86,21}, {87,21}, {88,22}, {89,22}, {90,22}, {91,22}, {92,23}, 
	{93,23}, {94,23}, {95,23}, {96,24}, {97,24}, {98,24}, {99,25}, {100,25}, {101,25}, {102,26}, {103,26}, {104,27}, {105,27}, {106,27}, {107,28}, 
	{108,28}, {109,29}, {110,29}, {111,30}, {112,30}, {113,31}, {114,31}, {115,32}, {116,32}

	};

	bool good(true);		//***SHOT Optimistic about results!
	// Insert trajectory data from flightPath array into the array above. Scale the height values then convert to INTs for array indexing.
	for (int i(0); i < maxDataPoints && flightPath[i][x] != dataEnd; i++)
	{
		int tempY = (pitchLevel - (int)(yScale*flightPath[i][y])); //***SHOT get Y coord 
		int tempX = (leftMargin + (int)(xScale*flightPath[i][x])); //***SHOT get X coord

		//***SHOT Check Y (height coordinate) value (no real need to check X value as they should be fixed increments, see "actualPoints", above.)
		if (tempY == actualPoints[i][y])
			graphDisplay[tempY][tempX] = ball;					//***SHOT The Y value is fine, plot it
		else {
			graphDisplay[tempY][tempX] = 'x';					//***SHOT The Y value is incorrect, plot as 'x'...
			graphDisplay[actualPoints[i][y]][tempX] = ball;		//***SHOT ...and plot correct one.
			good = false;											//***SHOT Not good!
		};

	}//***SHOT produce a speech bubble...
	if (good) graphDisplay[28].replace(7, 11, "LOOKS GOOD!");
	else	  graphDisplay[28].replace(10, 4, "DOH!");
	//***SHOT *****************************************************************************************************************

	int GoalPostXpos = (int)(xScale * distanceToGoal) + leftMargin; // absolute X coord of goal posts.

	// Insert goal post at required distance into array above.
	for (int i(0); i < (int)(goalPostHeight * yScale); i++)
	{
		graphDisplay[pitchLevel - i][GoalPostXpos] = goalPost;	// insert from ground upwards (watch it go up in a memory window!)
	};

	// Place the cross bar...
	graphDisplay[pitchLevel + 1 - (int)(crossBarHeight * yScale)][GoalPostXpos] = crossBar;

	// Produce the finished graph on the console...
	std::cout << YaxisTitle1 << '(' << yourName << ')' << YaxisTitle2;
	for (int i(0); i < graphLines; ++i) std::cout << graphDisplay[i];	// display the big picture...

	// Output enough spaces to get goal post title to correct position, aligning the '+' with the post...

	//#pragma omp parallel for schedule (static, repeats/NumThreads)
	for (int i(0); i < GoalPostXpos; i++) std::cout << SPACE; std::cout << goalPostTitle;

	// Last but not least...
	std::cout << XaxisTitle1 << yourTeamName << XaxisTitle2;

	//*************************************************************************************
#ifdef _trace	// Display/save generated data...

	ofstream flightData;
	flightData.open("FlightPathData.txt", ios::app);			// Black box recorder!
	flightData << "\n\nFlight path data for distance " << distanceToGoal << "m, angle= " << angle
		<< " degrees, and speed= " << speed << " m/s:";
	flightData << setw(6.2) << "\nFlight path: \t Screen Coords generated:\n";
	cout << setw(6.2) << "\n\nFlight path data and resulting screen coordinates:\n";

	for (int i(0); i < maxDataPoints && flightPath[i][x] != dataEnd; ++i)
	{
		cout << '{' << flightPath[i][x] << " \t" << flightPath[i][y] << "}  \t {" << (leftMargin + (int)(xScale*flightPath[i][x])) << " \t" << (pitchLevel - (int)(yScale*flightPath[i][y])) << "}\n ";
		flightData << '{' << flightPath[i][x] << " \t" << flightPath[i][y] << "}  \t {" << (leftMargin + (int)(xScale*flightPath[i][x])) << " \t" << (pitchLevel - (int)(yScale*flightPath[i][y])) << "}\n ";
	}
	flightData.close();

#endif //_trace

	logData(angle, speed);

	//*************************************************************************************
}


//*****************************************************************************************************************
void getDistanceToKick(float* distance)
{
	std::cout << "\nDistance to goal in metres, 5.0 mininmum, 50.0 maximum?: ";
	cin >> *distance;
	while (*distance < minDistanceToGoal || *distance > maxDistanceToGoal)
	{
		std::cout << "\nOutside acceptable range. Try again: ";  cin >> *distance;
	}
}

//*****************************************************************************************************************
template <class T>					// converts INTs and FLOATs to a string of chars
string ToString(T some_value)
{
	ostringstream textVersion;
	textVersion << some_value;
	return textVersion.str();
}

//*****************************************************************************************************************
//*** Timing
_inline unsigned __int64 GetTimeStamp(__int64 *ticks)
{
	_asm {
		rdtscp						// read Time Stamp Counter 
		mov ecx, [ticks]			// get address of parameter 'ticks'
		mov[ecx], eax				// result returned in EDX:EAX pair
		mov[ecx + 4], edx			// 'ticks' is 64 bits
	}
}





//*****************************************************************************************************************
//*** File stream

ofstream foutLog;
ifstream finLog;
const float firstSolutionTime = 0.00025303F;
const float firstFlightPathTime = 0.00015936F;

void logData(const float& angle, const float& speed)
{
	foutLog.open("Log2.txt", ios::app);
	if (foutLog.fail()) std::cout << "An Error has occurred when opening file";
	else
	{
		foutLog << "distance: " << yourKickDistance << "m\n";
		foutLog << "iterations: " << repeats << '\n';
		foutLog << "angle (degs): " << angle << '\n';
		foutLog << "speed (m/s): " << speed << '\n';
		foutLog << "Time to solution (us) = " << SpeedAndAngleTme/repeats << '\n';
		foutLog << "Time generating flight path (us) = " << GenerateFlightPathTime/repeats << '\n';
		foutLog << "Time to solution % speed up = " << -(((SpeedAndAngleTme / repeats) - firstSolutionTime) / firstSolutionTime) * 100 << '\n';
		foutLog << "Time generating flight path % speed up = " << -(((GenerateFlightPathTime / repeats) - firstFlightPathTime) / firstFlightPathTime) * 100 << '\n';
		foutLog << "----------------------------------------------------\n\n\n";
		foutLog.close();
	}

}


//************************************* END OF PROGRAM ************************************************************

